import react from '@vitejs/plugin-react-swc';
import tsconfigPaths from 'vite-tsconfig-paths'
import path from 'path';

// https://vitejs.dev/config/
export default () => {
  const root = process.cwd();

  return {
    base: '/',
    root,
    define: {
      globalThis: 'globalThis',
    },
    publicDir: false,
    emptyOutDir: true,
    server: {
      port: 9001,
      host: true,
    },
    resolve: {
      alias: {
        'server': path.resolve(__dirname, './src/server/'),
        'client': path.resolve(__dirname, './src/client/'),
        'store': path.resolve(__dirname, './src/client/store/'),
      },
    },
    plugins: [
      react(),
      tsconfigPaths()
    ],
    test: {
      globals: true,
      environment: 'jsdom',
      setupFiles: [
        "./setupVitest.js"
      ]
    },
  };
};
