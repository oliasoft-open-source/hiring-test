# Hiring Test Release Notes

## 2.7.0

- Updated Node.js version to 22.10.0 ([OW-18529](https://oliasoft.atlassian.net/browse/OW-18529))

## 2.6.0

- Updated logo & GUI library to match new branding ([OW-13062](https://oliasoft.atlassian.net/browse/OW-13062))

## 2.5.0

- Small housekeeping tasks to align closer with our other repos
  - TypeScript Redux API middleware ([OW-12623](https://oliasoft.atlassian.net/browse/OW-12623))
  - `react-router` upgraded
  - switch from `connected-react-router` to `redux-first-history`
  - small package upgrades
  - slight re-org of the client directory

## 2.4.0

- Setup Typescript configuration ([OW-12281](https://oliasoft.atlassian.net/browse/OW-12281))

## 2.3.0

- Switch to vite and vitest ([OW-12166](https://oliasoft.atlassian.net/browse/OW-12166))

## 2.2.0

- Switch to yarn ([OW-12274](https://oliasoft.atlassian.net/browse/OW-12274))

## 2.1.1

- Fix release CI/CD pipeline ([OW-11691](https://oliasoft.atlassian.net/browse/OW-11691))

## 2.1.0

- first publish of release notes
